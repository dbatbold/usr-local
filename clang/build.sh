#!/bin/sh

DEPS="cmake make g++ clang-dev libxml2-dev python unzip linux-headers"
GIT_HOST=https://git.llvm.org/git
LLVM_LIBCXX=llvm/projects/libcxx
LLVM_LIBCXXABI=llvm/projects/libcxxabi
LLVM_CLANG=llvm/tools/clang
LLVM_LLD=llvm/tools/lld

for dep in $DEPS; do
	apk info -e $dep || { echo "$dep missing"; exit 1; }
done

if [ ! -d llvm ]; then
	git clone $GIT_HOST/llvm || exit 1
fi
if [ ! -d $LLVM_LIBCXX ]; then
	git clone $GIT_HOST/libcxx $LLVM_LIBCXX || exit 1
fi
if [ ! -d $LLVM_LIBCXXABI ]; then
	git clone $GIT_HOST/libcxxabi $LLVM_LIBCXXABI || exit 1
fi
if [ ! -d $LLVM_CLANG ]; then
	git clone $GIT_HOST/clang $LLVM_CLANG || exit 1
fi
if [ ! -d $LLVM_LLD ]; then
	git clone $GIT_HOST/lld $LLVM_LLD || exit 1
fi

mkdir build
cd build || exit 1

if [ ! -e Makefile ]; then
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr/local/clang \
		-DCMAKE_C_COMPILER=clang \
		-DCMAKE_CXX_COMPILER=clang++ \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCLANG_ENABLE_BOOTSTRAP=ON \
		-DLLVM_TARGETS_TO_BUILD="X86;ARM" \
		-DLIBCXX_HAS_MUSL_LIBC=ON \
		-DBUILD_SHARED_LIBS=ON \
		../llvm 2>cmake.err | tee cmake.log
fi

CORES=`grep ^processor /proc/cpuinfo | sed 1d | wc -l`

time make -j $CORES \
	2>>make.err | tee -a make.log

[ -e bin/clang ] || exit 1

time make -j $CORES check-all \
	2>>make-check-all.err | tee -a make-check-all.log
